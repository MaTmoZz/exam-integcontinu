import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, precision_score, recall_score
import joblib

# Charger le fichier CSV dans un DataFrame
df = pd.read_csv("reviews_unique.csv")

# Afficher les premières lignes du DataFrame
print(df.head())

# Vérifier les dimensions du DataFrame
print("Dimensions du DataFrame :", df.shape)

# Vérifier s'il y a des valeurs manquantes
print("Valeurs manquantes :\n", df.isnull().sum())

# Afficher des statistiques descriptives
print("Statistiques descriptives :\n", df.describe())


