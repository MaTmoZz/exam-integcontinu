import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, precision_score, recall_score
import joblib

# Chargement du dataset
data = pd.read_csv('reviews_unique.csv')

# Exploration du dataset
print("Nombre de critiques : ", len(data))
print(data.head())

# Séparation des données en 2 catégories: critiques (X) et labels (y)
X = data['critiques']
y = data['labels']

# Vectorisation des textes
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(X)

# Séparation des données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entraînement du modèle Logistic Regression
model = LogisticRegression()
model.fit(X_train, y_train)

# Prédictions sur l'ensemble de test
y_pred = model.predict(X_test)

# Évaluation des performances du modèle
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)

print("Accuracy : ", accuracy)
print("Precision : ", precision)
print("Recall : ", recall)

# Sauvegarde du modèle entraîné
joblib.dump(model, 'trained_model.pkl')
