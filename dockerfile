# Utilisation d'une image Python officielle comme base
FROM python:3.9-slim

# Définition du répertoire de travail dans le conteneur
WORKDIR /app

# Copie des fichiers nécessaires dans le conteneur
COPY app.py .
COPY trained_model.pkl .
COPY requirements.txt .

# Installation des dépendances Python
RUN pip install --no-cache-dir -r requirements.txt

# Exposition du port 5000 pour accéder à l'API Flask
EXPOSE 5000

# Commande pour exécuter l'application Flask lors du démarrage du conteneur
CMD ["python", "app.py"]
