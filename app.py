from flask import Flask, request, jsonify
import joblib
from sklearn.feature_extraction.text import TfidfVectorizer

# Chargement du modèle
model = joblib.load('trained_model.pkl')
vectorizer = TfidfVectorizer()


# Création de l'application Flask
app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json()
    text = data['text']
    # Vectorisation du texte
    text_vectorized = vectorizer.transform([text])
    # Prédiction
    prediction = model.predict(text_vectorized)[0]
    return jsonify({'prediction': int(prediction)})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
